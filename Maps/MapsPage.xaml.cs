﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Maps
{
    public partial class MapsPage : ContentPage
    {
        public MapsPage()
        {
            InitializeComponent();

            PopulatePicker();
        }

        private void PopulatePicker()
        {
            Dictionary<string, Color> nameToColor = new Dictionary<string, Color>()
            {
                { "Satellite", Color.Aqua },
                { "Maps", Color.Gray },

            };


            foreach (var item in nameToColor)
            {
                SamplePicker.Items.Add(item.Key);
            }

            SamplePicker.SelectedIndex = 0;
        }
        void Handle_NavigateToMapSample(object sender, System.EventArgs e)
        {
            Navigation.PushModalAsync(new MapSample());
        }

        void Handle_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            // Code to handle user making index changes in picker
        }
    }
}